/* Copyright Applied Industrial Logic Limited 2017. All rights Reserved */
/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package com.ail.pageflow;

import static com.ail.core.CoreContext.getCoreProxy;
import static com.ail.pageflow.PageFlowContext.getPolicy;

import java.io.IOException;
import java.util.List;

import com.ail.core.BaseException;
import com.ail.core.CoreContext;
import com.ail.core.Type;
import com.ail.core.audit.Revision;
import com.ail.core.audit.RevisionType;
import com.ail.core.context.RequestWrapper;

/**
 * Page element to display the contents of the bound object's audit history
 */
public class AuditDetails extends PageElement {
    private static final long serialVersionUID = -4810599045554021748L;

    /**
     * If true the user is offered the ability to select a revision of the policy and move
     * the current pageflow to it.
     */
    private boolean selectable = false;

    public AuditDetails() {
        super();
    }

    public boolean isSelectable() {
        return selectable;
    }

    public void setSelectable(boolean selectable) {
        this.selectable = selectable;
    }

    public List<Revision> getPolicyRevisions() throws BaseException {
        return getCoreProxy().fetchRevisions(getPolicy());
    }

    public String getCurrentRevisionId() {
        return "current";
    }

    public String getCheckboxId() {
        return "revision";
    }

    public String getAffectedEntities(Revision revision) {
        StringBuffer res = new StringBuffer();

        for(RevisionType type: revision.getChanges().keySet()) {
            res.append(i18n(type.longName())).append(": ");
            for(Type entity: revision.getChanges().get(type)) {
                res.append(" ").append(entity.getClass().getSimpleName()).append("(").append(entity.getSystemId()).append(")");
            }
            res.append("; ");
        }

        return res.toString();
    }

    @Override
    public Type renderResponse(Type model) throws IllegalStateException, IOException {
        return executeTemplateCommand("AuditDetails", model);
    }

    @Override
    public Type processActions(Type model) throws BaseException {
        if (!isSelectable() || !conditionIsMet(model)) {
            return model;
        }

        RequestWrapper request = PageFlowContext.getRequestWrapper();

        if (request.getParameter(getCheckboxId()) != null) {
            String selection = (request.getParameter(getCheckboxId()));

            if (getCurrentRevisionId().equals(selection)) {
                CoreContext.setRevisionId(null);
            }
            else {
                try {
                    CoreContext.setRevisionId(Long.parseLong(selection));
                } catch(NumberFormatException e) {
                    CoreContext.getCoreProxy().logError("'"+selection+"' not recognised as a revision Id");
                }
            }
        }

        return model;
    }
}
