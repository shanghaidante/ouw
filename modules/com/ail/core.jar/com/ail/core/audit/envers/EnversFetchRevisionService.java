/* Copyright Applied Industrial Logic Limited 2018. All rights Reserved */
/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package com.ail.core.audit.envers;

import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;

import com.ail.annotation.ServiceImplementation;
import com.ail.core.PreconditionException;
import com.ail.core.Service;
import com.ail.core.Type;
import com.ail.core.audit.AuditRoot;
import com.ail.core.audit.FetchRevisionService.FetchRevisionArgument;
import com.ail.core.persistence.CreateException;
import com.ail.core.persistence.hibernate.HibernateSessionBuilder;

/**
 * An implementation of FetchRevisionService for Envers.
 */
@ServiceImplementation
public class EnversFetchRevisionService extends Service<FetchRevisionArgument> {

    @Override
    public void invoke() throws PreconditionException, CreateException {
        AuditReader auditReader = AuditReaderFactory.get(HibernateSessionBuilder.getSessionFactory().getCurrentSession());

        Type ret = auditReader.find(args.getClassArg(), args.getUIDArg(), args.getRevisionArg().intValue());

        if (AuditRoot.class.isAssignableFrom(args.getClassArg())) {
            ((AuditRoot)ret).setAuditRecord(true);
        }

        args.setTypeRet(ret);
    }
}
